#include "ROSAPIProvider.h"
#include <stdlib.h> 

#ifdef __cplusplus
extern "C" {
#endif
#include "ioboard_api_provider/servos_and_motors.h"
#include "ioboard_api_provider/psd_sensors.h"
#include "ioboard_api_provider/inout.h"
#ifdef __cplusplus
}
#endif

// Subclass our provider, the parent class creates the ROS Node
// we simply need to give it some information about how to access
// the various sensors the provider supports.
// In this case, we are just going to pull some values from thin
// air (randoms) but in a real IOBoard implementation it would be
// making calls to the eyebot.h library.
class IOBoardProvider: public ROSAPIProvider {
	
	public:


		// these must all be implemented, even if they return
		// junk / no data otherwise you'll get compiler errors
		// about missing virtual methods.
		virtual int getPSD(int id)
		{
			// IOBOARD Implementation
			return PSDGetRaw(id);

			// Mock IOBOARD Implementation
			//return rand() % 1000;
		}

		virtual int getAnalog(int id)
		{
			// IOBOARD Implementation
			return ANALOGRead(id);

			// Mock IOBOARD Implementation
			return rand() % 256;
		}

		virtual int getDigital(int id)
		{
			// IOBOARD Implementation
			return DIGITALRead(id);

			// Mock IOBOARD Implementation
			//return rand() % 10;
		}

		virtual void setMotor(int id, int percent)
		{
			//IOBOARD Implementation
			MOTORDriveRaw(id,percent);

			// Mock IOBOARD Implementation
			//ROS_INFO("setMotor() called with Id: %d, percent: %d", id, percent);
			return;
		}

		virtual void setServo(int id, int ticks)
		{
			//IOBOARD Implementation
			SERVOSet(id, ticks);

			// Mock IOBOARD Implementation
			//ROS_INFO("setServo() called with Id: %d, ticks: %d", id, ticks);
			return;
		}

		virtual void setDigital(int id, int value)
		{
			//IOBOARD Implementation
			DIGITALWrite(id, value);

			// Mock IOBOARD Implementation
			//ROS_INFO("setDigital() called with Id: %d, value: %d", id, value);
			return;
		}

		virtual void setupDigital(std::vector<int> readPins, std::vector<int> writePins)
		{
			//call the parent method
			ROSAPIProvider::setupDigital(readPins, writePins);

			for(std::vector<int>::iterator it = readPins.begin(); it != readPins.end(); ++it)
			{
				DIGITALSetup(*it, 'i');
			}

			for(std::vector<int>::iterator it = writePins.begin(); it != writePins.end(); ++it)
			{
				DIGITALSetup(*it, 'o');
			}

		}

};

int main(int argc, char **argv)
{
	// Init the name of our node, in this case we're mocking a USAL.
	ros::init(argc, argv, "USAL");

	// Create an instance of our provider class to make calls against.
	IOBoardProvider* ioboardHardwareController = new IOBoardProvider();

	// Depending on how our hardware is wired in, we need to tell the hardware controller
	// which pins we're using for our various PSDs, Digital and Analog IOs.

	// Setup the PSDs to read from pins 1,3 and 5.
	static const int psdPinsArr[] = {1,3,5};
	std::vector<int> pins (psdPinsArr, psdPinsArr + sizeof(psdPinsArr) / sizeof(psdPinsArr[0]) );
	ioboardHardwareController->setupPsd(pins);

	// Setup the analog pins such that we read from pins 2,4 and 6.
	static const int analogPins[] = {2,4,6};
	std::vector<int> analogVec (analogPins, analogPins + sizeof(analogPins) / sizeof(analogPins[0]) );
	ioboardHardwareController->setupAnalog(analogVec);

	// Digital Pins can be Read and Write so we need to say which is which.
	// Setup pins 7,8 and 9 to be reaad pins.
	static const int digitalPins[] = {7,8,9};
	std::vector<int> digitalReadVec (digitalPins, digitalPins + sizeof(digitalPins) / sizeof(digitalPins[0]) );

	// Setup pins 4 and 5 to be write-able pins.
	static const int digitalWritePins[] = {4,5};
	std::vector<int> digitalWriteVec (digitalWritePins, digitalWritePins + sizeof(digitalWritePins) / sizeof(digitalWritePins[0]) );
	// Make the call to the digitalSetup method, passing in our two vectors.
	ioboardHardwareController->setupDigital(digitalReadVec, digitalWriteVec);

	// Set the global loop rate - this should be the lowest common denominator from all our components.
	// If the PSDs for example have a resolution of 1Hz, then we set this to 1 - Once per second.
	ioboardHardwareController->setLoopRate(1);

	// Here we start the run loop and let the Provider parent class takeover.
	ioboardHardwareController->startRunLoop();
}

