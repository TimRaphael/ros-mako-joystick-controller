#include "ROSAPIConsumer.h"

ROSAPIConsumer::ROSAPIConsumer()
{

	//setup publishers
	m_motorPub = m_nodeHandle.advertise<ioboard_api_provider::motor>(ROSAPITopics::MOTOR_TOPIC, 1000);
	m_servoPub = m_nodeHandle.advertise<ioboard_api_provider::servo>(ROSAPITopics::SERVO_TOPIC, 1000);
	m_digitalWritePub = m_nodeHandle.advertise<ioboard_api_provider::digital>(ROSAPITopics::DIGITAL_WRITE_TOPIC, 1000);

	// setup subscribers
	m_psdSub 			= m_nodeHandle.subscribe(ROSAPITopics::PSD_TOPIC, 1000, &ROSAPIConsumer::psdCallback, this);
	m_analogSub			= m_nodeHandle.subscribe(ROSAPITopics::ANALOG_TOPIC, 1000, &ROSAPIConsumer::analogCallback, this);
	m_digitalReadSub	= m_nodeHandle.subscribe(ROSAPITopics::DIGITAL_READ_TOPIC, 1000, &ROSAPIConsumer::digitalCallback, this);

}

void ROSAPIConsumer::psdCallback(const ioboard_api_provider::psd& msg)
{
	//ROS_INFO("PSDCallback");
	this->recievedPsdData(msg.id, msg.data);
	return;
}

void ROSAPIConsumer::analogCallback(const ioboard_api_provider::analog& msg)
{
	//ROS_INFO("AnalogCallback");
	this->recievedAnalogData(msg.id, msg.data);
	return;
}

void ROSAPIConsumer::digitalCallback(const ioboard_api_provider::digital& msg)
{
	//ROS_INFO("Digital Callback");
	this->recievedDigitalData(msg.id, msg.data);
	return;
}

void ROSAPIConsumer::setMotor(int id, int percent)
{
	ioboard_api_provider::motor motorMsg;
	motorMsg.data = percent;
	motorMsg.id = id;

	m_motorPub.publish(motorMsg);
}

void ROSAPIConsumer::setServo(int id, int ticks)
{
	ioboard_api_provider::servo servoMsg;
	servoMsg.data = ticks;
	servoMsg.id = id;

	m_servoPub.publish(servoMsg);
}

void ROSAPIConsumer::setDigital(int id, int value)
{
	ioboard_api_provider::digital digitalMsg;
	digitalMsg.data = value;
	digitalMsg.id = id;

	m_digitalWritePub.publish(digitalMsg);
}
