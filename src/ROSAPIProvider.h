#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "ioboard_api_provider/digital.h"
#include "ioboard_api_provider/psd.h"
#include "ioboard_api_provider/analog.h"
#include "ioboard_api_provider/motor.h"
#include "ioboard_api_provider/servo.h"
#include "ROSAPITopics.h"
#include <vector>

class ROSAPIProvider
{
	protected:
		ros::NodeHandle m_nodeHandle;

		static const int NUM_DIGITAL_PINS = 20;
		static const int NUM_PSD = 6;
		static const int NUM_ANALOG_PINS = 16;

		// Publishers
		ros::Publisher m_motorPub;
		ros::Publisher m_servoPub;
		ros::Publisher m_psdPub;
		ros::Publisher m_digitalReadPub;
		ros::Publisher m_analogPub;
		ros::Publisher m_imuPub;

		// Subscribers
		ros::Subscriber m_motorSub;
		ros::Subscriber m_servoSub;
		ros::Subscriber m_psdSub;
		ros::Subscriber m_digitalWriteSub;
		ros::Subscriber m_analogSub;
		ros::Subscriber m_imuSub;

		// callbacks
		void motorCallback(const ioboard_api_provider::motor& msg);
		void servoCallback(const ioboard_api_provider::servo& msg);
		void digitalWriteCallback(const ioboard_api_provider::digital& msg);
		void imuCallback(const sensor_msgs::Imu& msg);
		

		// setup the services
		//ros::ServiceServer m_digitalSetup;

		// setup the looping rates
		int m_loopRate;

		// PSDs to Read From
		std::vector<int> m_psdIds;

		// Analog Pins to read from
		std::vector<int> m_analogPins;

		// Digital Pins to read from
		std::vector<int> m_digitalReadPins;

		// Digital Pins to write to
		std::vector<int> m_digitalWritePins;


	public:

		ROSAPIProvider();

		void setupSubscribers();

		void setLoopRate(int rate) { this->m_loopRate = rate; };

		void startRunLoop();

		// methods to do setup of sensors
		virtual void setupPsd(std::vector<int> pins) { this->m_psdIds = pins; }
		virtual void setupDigital(std::vector<int> readPins, std::vector<int> writePins) { this->m_digitalReadPins = readPins; this->m_digitalWritePins = writePins; }
		virtual void setupAnalog(std::vector<int> pins) { this->m_analogPins = pins; }

		// methods to fetch data from implementation
		virtual int getPSD(int id) = 0;
		virtual int getAnalog(int id) = 0;
		virtual int getDigital(int id) = 0;
		//virtual sensor_msgs::Imu getIMU();

		// method to perform actions from the implementation
		virtual void setMotor(int id, int percent) { ROS_INFO("setMotor called from parent"); }
		virtual void setServo(int id, int ticks) = 0;
		virtual void setDigital(int id, int value) = 0;
};