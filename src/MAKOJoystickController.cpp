#include "ROSAPIConsumer.h"
#include "sensor_msgs/Joy.h"

// Subclass our Consumer class so we can respond to async events.
class MAKOJoystickController: public ROSAPIConsumer {

	public:
	
		// These are the callback functions for our various sensors
		// These will be called when new data comes from any sensor
		// id = the number of the sensor and the data is the int value.
		// You will only recieve callbacks for the sensors that have been "setup"
		// in the Provider sub-class.
		// These must all be implemented for now, otherwise you'll get compiler errors
		// about missing abstract class methods (virtuals).
		virtual void recievedPsdData(int id, int data)
		{
			//ROS_INFO("Recieved PSD Data with id: %d data: %d", id, data);
			return;
		}

		virtual void recievedAnalogData(int id, int data)
		{
			//ROS_INFO("Recieved Analog Data with id: %d data: %d", id, data);
			return;
		}

		virtual void recievedDigitalData(int id, int value)
		{
			//ROS_INFO("recievedDigitalData with id: %d value: %d", id, value);
			return;
		}

};

struct makoMotors {

	int front;
	int back;
	int left;
	int right;

};


makoMotors m_makoMotorSpeeds;
makoMotors m_makoMotorSpeedsChanged;

const int LEFT_MOTOR = 1;
const int RIGHT_MOTOR = 2;
const int FRONT_MOTOR = 3;
const int BACK_MOTOR = 4;

const int DIVE_SPEED = 50;


void joystick_callback(const sensor_msgs::Joy::ConstPtr& msg)
{
	//sticks -1.0 <= 0 <= 1.0
	//left stick = msg->axes[1]
	//right stick = msg->axes[4]


	// for each message, work out the speed it's currently going, update the struct and if it's changed - issue the motor change command.
	int _lCtrlSpeed = 0;
	int _rCtrlSpeed = 0;

	// we're only supporting forward!
	if(msg->axes[1] >= 0)
	{	
		ROS_INFO("Checked Left stick.");
		//left motor with the left JS
		//_lCtrlSpeed = ((int)(msg->axes[1] * 100) - (msg->axes[1] % 10));
		_lCtrlSpeed = (int)(msg->axes[1] * 10) * 10;
		// 0.12345 --> 12.345 --> 12 --> 10;

		if(_lCtrlSpeed != m_makoMotorSpeeds.left)
		{
			m_makoMotorSpeeds.left = _lCtrlSpeed;
			m_makoMotorSpeedsChanged.left = 1;
		}
	}

	if(msg->axes[4] >= 0)
	{
		ROS_INFO("Checked Right stick.");
		//right motor with the right JS
		//_rCtrlSpeed = ((int)(msg->axes[4] * 100) - (msg->axes[4] % 10));
		_rCtrlSpeed = (int)(msg->axes[4] * 10) * 10;
		
		if(_rCtrlSpeed != m_makoMotorSpeeds.right)
		{
			m_makoMotorSpeeds.right = _rCtrlSpeed;
			m_makoMotorSpeedsChanged.right = 1;
		}
	}

	if(msg->buttons[0] && m_makoMotorSpeeds.front != DIVE_SPEED) //if on and we're not diving.
	{
		m_makoMotorSpeeds.back = DIVE_SPEED;					// start diving
		m_makoMotorSpeeds.front = DIVE_SPEED;

		m_makoMotorSpeedsChanged.front = 1;						// flag for change.
		m_makoMotorSpeedsChanged.back = 1;
	}

	if(!msg->buttons[0] && m_makoMotorSpeeds.front == DIVE_SPEED) //if off and we're diving.
	{
		m_makoMotorSpeeds.back = 0;								 // stop diving.
		m_makoMotorSpeeds.front = 0;

		m_makoMotorSpeedsChanged.front = 1;						 // flag for change
		m_makoMotorSpeedsChanged.back = 1;
	}


	// check if button state has changed
	// if(msg->buttons[0] != m_makoMotorSpeedsChanged.front) //A
	// {
	// 	ROS_INFO("Changed A Button.");
	// 	if(msg->buttons[0])
	// 	{
	// 		m_makoMotorSpeeds.back = 50;
	// 		m_makoMotorSpeeds.front = 50;

	// 		m_makoMotorSpeedsChanged.front = 1;
	// 		m_makoMotorSpeedsChanged.back = 1;
	// 	}
	// 	else
	// 	{
	// 		m_makoMotorSpeeds.back = 0;
	// 		m_makoMotorSpeeds.front = 0;

	// 		m_makoMotorSpeedsChanged.front = 0;
	// 		m_makoMotorSpeedsChanged.back = 0;
	// 	}
	// }

	//if(msg->buttons[3]) //Y
}

int main(int argc, char **argv)
{
	// As we're going to simply be calling methods on our controller, we need
	// to setup a ROS node for our control code.
	ros::init(argc, argv, "MAKOJoystickController");

	ros::NodeHandle m_nodeHandle;


	// Init a usalController instance from which we can issue commands.
	MAKOJoystickController* makoController = new MAKOJoystickController();

	// set our loop rate to 1Hz
	ros::Rate loop_rate(10);

	m_makoMotorSpeeds.front = 0;
	m_makoMotorSpeeds.back = 0;
	m_makoMotorSpeeds.left = 0;
	m_makoMotorSpeeds.right = 0;

	m_makoMotorSpeedsChanged.front = 0;
	m_makoMotorSpeedsChanged.back = 0;
	m_makoMotorSpeedsChanged.left = 0;
	m_makoMotorSpeedsChanged.right = 0;



	// subscribe to the joystick topic
	ros::Subscriber m_joysticksub;
	m_joysticksub = m_nodeHandle.subscribe("/joy", 1000, &joystick_callback);


	// Start our control / main run loop.
	while(ros::ok())
	{
		// needed to allow interrupts for callbacks from sensors
		ros::spinOnce();

		if(m_makoMotorSpeedsChanged.left)
		{
			ROS_INFO("Updating Left Motor Speed to: %d", m_makoMotorSpeeds.left);
			makoController->setMotor(LEFT_MOTOR, m_makoMotorSpeeds.left);
			m_makoMotorSpeedsChanged.left = 0;
		}

		if(m_makoMotorSpeedsChanged.right)
		{
			ROS_INFO("Updating Right Motor Speed to: %d", m_makoMotorSpeeds.right);
			makoController->setMotor(RIGHT_MOTOR, m_makoMotorSpeeds.right);
			m_makoMotorSpeedsChanged.right = 0;
		}

		if(m_makoMotorSpeedsChanged.front)
		{
			ROS_INFO("Diving at: %d", m_makoMotorSpeeds.front);
			makoController->setMotor(FRONT_MOTOR, m_makoMotorSpeeds.front);
			makoController->setMotor(BACK_MOTOR, m_makoMotorSpeeds.back);

			m_makoMotorSpeedsChanged.front = 0;
			m_makoMotorSpeedsChanged.back = 0;
		}

		// to control the sleep phase of the loop rate.
		loop_rate.sleep();
	}
}